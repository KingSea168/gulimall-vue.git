# gulimall-vue

#### 介绍
该项目是谷粒商城-前端项目，前后端分离

后台项目[gulimall](https://gitee.com/KingSea168/gulimall.git)

本项目持续更新，持续完善，欢迎大家star or fork~

#### 使用说明

1.  npm config set registry http://registry.npm.taobao.org/
2.  npm install
3.  npm run dev

#### 技术交流
项目技术文档，请关注我们【老吴流量】

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/155641_f63ee664_1106489.jpeg "qrcode_for_gh_178a5832e433_344.jpg")