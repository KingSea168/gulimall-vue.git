/**
 * 开发环境
 */
;(function () {
  window.SITE_CONFIG = {};

  // api接口请求地址：原来向后台系统发请求，由于微服务，改为向网关发请求
  // window.SITE_CONFIG['baseUrl'] = 'http://localhost:8080/renren-fast';
  window.SITE_CONFIG['baseUrl'] = 'http://192.168.0.102:88/api';
  // window.SITE_CONFIG['baseUrl'] = 'http://kinggateway.ngrok2.xiaomiqiu.cn/api';

  // cdn地址 = 域名 + 版本号
  window.SITE_CONFIG['domain']  = './'; // 域名
  window.SITE_CONFIG['version'] = '';   // 版本号(年月日时分)
  window.SITE_CONFIG['cdnUrl']  = window.SITE_CONFIG.domain + window.SITE_CONFIG.version;
})();
